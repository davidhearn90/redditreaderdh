package android.hearnd.redditreader;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class RedditPostAdapter extends RecyclerView.Adapter<RedditPostHolder> {

    private ArrayList<RedditPost> redditposts;
    private ActivityCallback activityCallback;

    public RedditPostAdapter(ActivityCallback activityCallback){
        redditposts = RedditPostParser.getInstance().redditposts;
        this.activityCallback = activityCallback;
    }

    @Override
    public RedditPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new RedditPostHolder(view);
    }

    @Override
    public void onBindViewHolder(RedditPostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri redditUri = Uri.parse(redditposts.get(position).url);
                activityCallback.onPostSelected(redditUri);
            }
        });

        holder.titleText.setText(redditposts.get(position).title);
    }

    @Override
    public int getItemCount() {
return redditposts.size();
        }
        }
