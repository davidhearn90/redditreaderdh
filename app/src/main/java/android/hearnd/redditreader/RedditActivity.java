package android.hearnd.redditreader;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;


public class RedditActivity extends SingleFragmentActivity implements ActivityCallback {

    @Override
    protected Fragment createFragment() {
        return new RedditListFragment();
    }


    @Override
    public void onPostSelected(Uri redditPostUri) {

        Intent intent = new Intent(getApplicationContext(), RedditWebActivity.class);
        intent.setData(redditPostUri);
        startActivity(intent);
    }
}
